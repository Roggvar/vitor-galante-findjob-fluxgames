using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    #region Variables
    private static bool gameIsPaused = false; // check if the game is paused or not
    public GameObject pauseMenuUI; // UI object
                                   // Instead of a public GameObject cound have used the find method and used a private variable
    #endregion

    #region Default Methods
    void Start()
    {
        gameIsPaused = false;
        Time.timeScale = 1f;
    }

    void Update()
    {
        // Pauses the game when the esc key is pressed
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(gameIsPaused == true) // If the game is paused
            {
                Resume(); // Calls for the resume method
            }
            else // if not
            {
                Pause(); // Calls for the pause method
            }
        }
    }
    #endregion

    #region Custom Methods
    private void Resume ()
    {
        pauseMenuUI.SetActive(false); // Disables the pause menu UI
        Time.timeScale = 1f;
        AudioListener.pause = false; // Resumes the game sounds
        gameIsPaused = false;
    }

    private void Pause ()
    {
        pauseMenuUI.SetActive(true); // Activate the pause menu UI
        Time.timeScale = 0f;
        AudioListener.pause = true; // Pauses the game sounds
        gameIsPaused = true;
    }
    #endregion
}