using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveGame
{

    public static void saveGame(int level) {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerCurrentStageSave.save";

        FileStream stream = new FileStream(path, FileMode.Create);

        SaveData data = new SaveData(level);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static SaveData loadGame() {
        string path = Application.persistentDataPath + "/playerCurrentStageSave.save";

        if(File.Exists(path)) {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            SaveData data = formatter.Deserialize(stream) as SaveData;
            stream.Close();

            return data;
        } else {
            return null;
        }
    }

}
