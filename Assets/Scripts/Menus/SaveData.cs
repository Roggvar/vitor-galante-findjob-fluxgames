using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{

    public int currentLevelSave;

    public SaveData(int currentLevel) {
        currentLevelSave = currentLevel;
    }

}
