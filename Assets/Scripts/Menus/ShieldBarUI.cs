using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldBarUI : MonoBehaviour
{
    public Slider slider;

    public void setShield(int shield) {
        slider.value = shield;
    }
}
